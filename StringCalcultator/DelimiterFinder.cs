﻿
using System.Linq;
using System.Reflection.Metadata.Ecma335;

namespace StringCalculator
{
    public class DelimiterFinder : IDelimiterFinder
    {
        public string[] FindDelimiters(string numbers)
        {
            string[] delimiters = {",","\n"};

            if (numbers.StartsWith("##")) 
            {
                numbers = numbers.Substring(2);

                int numbersStartPosition = numbers.IndexOf(numbers.First());

                int numbersNewlinePosition = numbers.IndexOf("\n");

                string customDelimiter = CheckDelimiterForInputStartingWithHash(numbers, numbersStartPosition, numbersNewlinePosition);

                delimiters = delimiters.Append(customDelimiter).ToArray();

                if (numbers.Contains("][") || numbers.Contains('['))
                {
                    string[] moreCustomDelimiters = ExtractMoreCustomDelimiters(numbers, numbersStartPosition, numbersNewlinePosition);

                    return moreCustomDelimiters;
                }

                return delimiters;
            }

            return delimiters;
        }

        private string CheckDelimiterForInputStartingWithHash(string input, int inputStartPosition, int inputNewlinePosition) 
        {
            string customDelimiter = input.Substring(inputStartPosition, inputNewlinePosition - inputStartPosition);

            return customDelimiter;
        }

        private string[] ExtractMoreCustomDelimiters(string input, int inputStartPosition, int inputNewlinePosition) 
        {
            string trimmedInputSection = input.Substring(inputStartPosition, inputNewlinePosition);

            string sectionWithCustomDelimiters = trimmedInputSection.Trim('[',']');

            string separator = "][";

            string[] extractedDelimiters = sectionWithCustomDelimiters.Split(separator.ToArray(), StringSplitOptions.RemoveEmptyEntries);

            return extractedDelimiters;
        }
    }
}
