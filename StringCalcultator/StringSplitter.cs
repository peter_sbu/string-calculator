﻿
namespace StringCalculator
{
    public class StringSplitter : IStringSplitter
    {
        public string[] SplitString(string input, string[] delimiters)
        {
            string[] splittedStringArray;

            splittedStringArray = input.Split(delimiters,StringSplitOptions.None);

            if(input.StartsWith("##"))
            {
                splittedStringArray = input.Split("\n", StringSplitOptions.None);

                splittedStringArray = splittedStringArray[1].Split(delimiters,StringSplitOptions.None);

                return splittedStringArray;
            }

            return splittedStringArray;
        }
    }
}
