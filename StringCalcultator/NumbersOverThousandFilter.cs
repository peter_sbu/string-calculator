﻿
namespace StringCalculator
{
    public class NumbersOverThousandFilter : INumbersOverThousandFilter
    {
        IConverter converter = new Converter();
       public string IgnoreNumbersOverThousand(string[] numbersArray)
        {
            List<string> filteredNumbers = new List<string>();

            foreach (string number in numbersArray) 
            {
                string numbersOnly = converter.ConvertLettersToNumbers(number);

                if (int.Parse(numbersOnly) > 1000)
                {
                    filteredNumbers.Add(number);
                }
            }
            if (filteredNumbers.Count > 0)
            {
                string numbersOverThousand = String.Join(",", filteredNumbers.ToArray());

                throw new Exception($"Numbers bigger than 1000 are not allowed, {numbersOverThousand}.");
            }

            return filteredNumbers.ToString();
        }
    }
}
