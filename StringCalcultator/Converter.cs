﻿
namespace StringCalculator
{
    public class Converter : IConverter
    {
        public int[] ConvertStringToInt(string[] numbers)
        {
            int[] convertedNumbers = {};

            foreach (string number in numbers)
            {
                string numbersOnly = ConvertLettersToNumbers(number);

                int intergerNumber = Convert.ToInt32(numbersOnly);

                convertedNumbers = convertedNumbers.Append(intergerNumber).ToArray();
            }

            return convertedNumbers;
        }

        public string ConvertLettersToNumbers(string number)
        {
            int minimumASCIICode = 97;

            int maximumASCIICode = 106;

            if (number.Length > 1 || !(char.Parse(number) >= minimumASCIICode && char.Parse(number) <= maximumASCIICode))
            {
                return number;
            }
           
            int convertedNumber = char.Parse(number) - minimumASCIICode;

            return convertedNumber.ToString();
        }

    }
}
