﻿

namespace StringCalculator
{
    public interface IStringSplitter
    {
        string[] SplitString(string input, string[] delimiters);
    }
}
