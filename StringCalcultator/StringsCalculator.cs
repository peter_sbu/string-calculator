﻿namespace StringCalculator
{
    public class StringsCalculator : IStringsCalculator
    {
        private IDelimiterFinder delimiterFinder;
        private IStringSplitter stringSplitter;
        private INumbersOverThousandFilter numbersOverThousandFilter;
        private IConverter converter;
        public StringsCalculator(IDelimiterFinder _delimiterFinder,IStringSplitter _stringSplitter, INumbersOverThousandFilter _numbersOverThousandFilter, 
            IConverter _converter)
        {
            delimiterFinder = _delimiterFinder;

            stringSplitter = _stringSplitter;

            numbersOverThousandFilter = _numbersOverThousandFilter;

            converter = _converter;
        }

        public int Subtract(string numbers)
        {
            if (String.IsNullOrEmpty(numbers))
            {
                return 0;
            }
            
            string[] delimiters = delimiterFinder.FindDelimiters(numbers);

            string[] splittedNumbers = stringSplitter.SplitString(numbers, delimiters);

            numbersOverThousandFilter.IgnoreNumbersOverThousand(splittedNumbers);

            int[] convertedNumbers = converter.ConvertStringToInt(splittedNumbers);

            return Calculate(convertedNumbers);
        }

        public int Calculate(int[] numbers)
        {
            int result = 0;
            int newNumber;
            foreach (int number in numbers)
            {
                if(number < 0)
                {
                    return result -= (number * -1);
                }

               result -= number;
            }
                    
           return result;
         }
    }
}