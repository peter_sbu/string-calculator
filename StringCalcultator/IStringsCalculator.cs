﻿
namespace StringCalculator
{
    public interface IStringsCalculator
    {
        int Subtract(string numbers);

        int Calculate(int[] numbers);
    }
}
