﻿
namespace StringCalculator
{
    public interface IConverter
    {
        int[] ConvertStringToInt(string[] numbers);

        string ConvertLettersToNumbers(string number);
    }
}
