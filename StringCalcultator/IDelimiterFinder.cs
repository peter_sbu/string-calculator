﻿
namespace StringCalculator
{
    public interface IDelimiterFinder
    {
        string[] FindDelimiters(string input);
    }
}
