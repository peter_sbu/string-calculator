﻿
namespace StringCalculator
{
    public interface INumbersOverThousandFilter
    {
        string IgnoreNumbersOverThousand(string[] numbers);
    }
}
