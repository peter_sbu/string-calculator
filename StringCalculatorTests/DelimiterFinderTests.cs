﻿
using StringCalculator;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace StringCalculatorTests
{
    public class DelimiterFinderTests
    {
        [Test]
        public void GIVEN_StringInputWithStandardDelimiters_WHEN_FindingDelimiters_RETURN_standardDelimiters()
        {
            //Arrange
            DelimiterFinder delimiterFinder = new DelimiterFinder();
            string input = "1,2";
            string[] expectedResult = {",","\n"};

            //Act
            string[] actualResult = delimiterFinder.FindDelimiters(input);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringInputWithCustomDelimiter_WHEN_FindingDelimiters_RETURN_ArrayOfDelimiters()
        {
            //Arrange
            DelimiterFinder delimiterFinder = new DelimiterFinder();
            string input = "##;\n1;2";
            string[] expectedResult = {",","\n",";"};

            //Act
            string[] actualResult = delimiterFinder.FindDelimiters(input);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringInputWithMoreCustomDelimiters_WHEN_FindingDelimiters_RETURN_ArrayOfDelimiters()
        {
            //Arrange
            DelimiterFinder delimiterFinder = new DelimiterFinder();
            string input = "##[***]\n1***2***3";
            string[] expectedResult = {"***"};

            //Act
            string[] actualResult = delimiterFinder.FindDelimiters(input);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringInputWithDifferentCustomDelimiters_WHEN_FindingDelimiters_RETURN_ArrayOfDelimiters()
        {
            //Arrange
            DelimiterFinder delimiterFinder = new DelimiterFinder();
            string input = "##[*][%]\n1*2%3";
            string[] expectedResult = {"*","%"};

            //Act
            string[] actualResult = delimiterFinder.FindDelimiters(input);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
