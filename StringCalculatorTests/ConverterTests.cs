﻿
using StringCalculator;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace StringCalculatorTests
{
    [TestFixture]
    public class ConverterTests
    {
        [Test]
        public void GIVEN_StringNumbers_WHEN_Converting_RETURN_Integers()
        {
            //Arrange
            Converter converter = new Converter();
            string[] numbers = {"1","2","3"};
            int[] expectedResult = {1,2,3};

            //Act
            int[] actualResult = converter.ConvertStringToInt(numbers);
        
            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringNumbersWuthLetters_WHEN_Converting_RETURN_Integers()
        {
            //Arrange
            Converter converter = new Converter();
            string numbers = "i";
            string expectedResult = "8";

            //Act
            string actualResult = converter.ConvertLettersToNumbers(numbers);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
