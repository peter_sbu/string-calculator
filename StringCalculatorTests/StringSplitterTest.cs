﻿
using StringCalculator;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace StringCalculatorTests
{
    public class StringSplitterTest
    {
        [Test]
        public void GIVEN_StringInput_WHEN_Splitting_RETURN_StringArrayContainingNumbers()
        {
            //Arrange
            StringSplitter stringSplitter = new StringSplitter();
            string input = "1,2";
            string[] delimiters = {",","\n"};
            string[] expectedResult = {"1","2"};

            //Act
            string[] actualResult = stringSplitter.SplitString(input, delimiters);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringInputWithNewline_WHEN_Splitting_RETURN_StringArrayContainingNumbers()
        {
            //Arrange
            StringSplitter stringSplitter = new StringSplitter();
            string input = "1\n2,3";
            string[] delimiters = {",","\n"};
            string[] expectedResult = {"1","2","3"};

            //Act
            string[] actualResult = stringSplitter.SplitString(input, delimiters);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringInputWithCustomDelimiters_WHEN_Splitting_RETURN_StringArrayContainingNumbers()
        {
            //Arrange
            StringSplitter stringSplitter = new StringSplitter();
            string input = "##;\n1;2";
            string[] delimiters = {",","\n",";"};
            string[] expectedResult = {"1","2"};

            //Act
            string[] actualResult = stringSplitter.SplitString(input, delimiters);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }
    }
}
