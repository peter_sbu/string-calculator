
using StringCalculator;
using NUnit.Framework;
using Assert = NUnit.Framework.Assert;

namespace StringCalculatorTests
{
    [TestFixture]
    public class StringCalculatorUnitTests
    {
        private IStringsCalculator stringsCalculator;
        private IDelimiterFinder delimiterFinder = new DelimiterFinder();
        private IStringSplitter stringSplitter = new StringSplitter();
        private INumbersOverThousandFilter numbersOverThousandFilter = new NumbersOverThousandFilter();
        private IConverter converter = new Converter();

        [SetUp]
        public void SetUp()
        {
            stringsCalculator = new StringsCalculator(delimiterFinder, stringSplitter, numbersOverThousandFilter, converter);
        }

        [Test]
        public void GIVEN_EmptyInput_WHEN_Subtracting_RETURN_Zero()
        {
            //Arrange
            string input = "";
            int expectedResult = 0;

            //Act
            int actualResult = stringsCalculator.Subtract(input);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringNumber_WHEN_Subtracting_RETURN_TheNumberAsNegative()
        {
            //Arrange

            string input = "1";
            int expectedResult = -1;

            //Act
            int actualResult = stringsCalculator.Subtract(input);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringInputWithTwoNumbers_WHEN_Subtracting_RETURN_TheirSumAsNegative()
        {
            //Arrange
            string input = "1,2";
            int expectedResult = -3;

            //Act
            int actualResult = stringsCalculator.Subtract(input);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringInputWithVariousNumbers_WHEN_Subtracting_RETURN_TheirSumAsNegative()
        {
            //Arrange
            string input = "1,4,6,8,2";
            int expectedResult = -21;

            //Act
            int actualResult = stringsCalculator.Subtract(input);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringNumbersWithNewline_WHEN_Subtracting_RETURN_TheirSumAsNegative()
        {
            //Arrange
            string input = "1\n2,3";
            int expectedResult = -6;

            //Act
            int actualResult = stringsCalculator.Subtract(input);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringNumberWithValuesOverThousand_WHEN_Filtering_RETURN_ThrowException()
        {
            //Arrange
            string input = "2023,1,2,1001";
            string expectedResult = "Numbers bigger than 1000 are not allowed, 2023,1001.";

            //Act
            var actualResult = Assert.Throws<Exception> (() => stringsCalculator.Subtract(input));

            //Assert
            Assert.AreEqual(expectedResult, actualResult.Message);
        }

        [Test]
        public void GIVEN_StringNumbersWithMoreCustomDelimiters_WHEN_Subtracting_RETURN_TheirSumAsNegative()
        {
            //Arrange
            string input = "##[***]\n1***2***3";
            int expectedResult = -6;

            //Act
            int actualResult = stringsCalculator.Subtract(input);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringNumbersWithMultipleCustomDelimiters_WHEN_Subtracting_RETURN_TheirSumAsNegative()
        {
            //Arrange
            string input = "##[*][%]\n1*2%3";
            int expectedResult = -6;

            //Act
            int actualResult = stringsCalculator.Subtract(input);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringNumbersWithVariousCustomDelimiters_WHEN_Subtracting_RETURN_TheirSumAsNegative()
        {
            //Arrange
            string input = "##[@*][%!]\n1@*2%!3";
            int expectedResult = -6;

            //Act
            int actualResult = stringsCalculator.Subtract(input);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringNumberWithNegativeNumbers_WHEN_IgnoringNegatives_RETURN_TheirSumAsNegative()
        {
            //Arrange
            string input = "1,2,-5";
            int expectedResult = -8;

            //Act
            int actualResult = stringsCalculator.Subtract(input);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_StringNumbersWithMoreCustomDelimitersAndLetters_WHEN_Subtracting_RETURN_TheirSumAsNegative()
        {
            //Arrange
            string input = "##[***]\n1***f***e";
            int expectedResult = -10;

            //Act
            int actualResult = stringsCalculator.Subtract(input);

            //Assert
            Assert.AreEqual(expectedResult, actualResult);
        }

    }
}