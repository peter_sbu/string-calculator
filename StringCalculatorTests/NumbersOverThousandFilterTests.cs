﻿
using StringCalculator;
using NUnit.Framework;
using Asser = NUnit.Framework.Assert;

namespace StringCalculatorTests
{
    public class NumbersOverThousandFilterTests
    {
        [Test]
        public void GIVEN_InputWithNumbersOverThousand_WHEN_Filtering_RETURN_ArrayOfIntegersWithoutOverThousandValues()
        {
            //Arange
            NumbersOverThousandFilter numbersOverThousand = new NumbersOverThousandFilter();
            string[] input = {"1003", "1", "9", "1001"};
            string expectedResult = "Numbers bigger than 1000 are not allowed, 1003,1001.";

            //Action
            var actualResult = Assert.Throws<Exception>(() => numbersOverThousand.IgnoreNumbersOverThousand(input));

            //Assert
            Assert.AreEqual(actualResult.Message, expectedResult);

        }

    }
}
