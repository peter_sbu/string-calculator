# String Calculator 

*Overview*

This program consumes a string of numbers that is delimited with various characters and does the following computations: 

- Finds the delimiters
- Splits the string 
- Filters numbers over 1000
- Converts the separated strings to integers if there are letters, they are converted to integers
- Performs an arithmatic operation that returns a negative integer

In the test project folder *StringCalculatorTests* see the tests covered for the above computations in the file *StringCalculatorUnitTests.cs*

*Objective*

The aim with this program was to learn and implement SOILID principles, specifically Single Responsibility, Interfaces Segregation and Dependency Inversion

*Tech Stack*
- .Net 6 Framework
- Microsoft Test Project with NUnit Framework
- C# 
